package com.ks;

/**
 * Hello world!
 */

import java.sql.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class App
{

    static String sqlmxQuery = "";
    static Connection sqlmxConn = null;


    public static void main(String[] args)
    {

        System.out.println("Programa para validar funcionamiento de MXSQL - NSSQL");

        PoolConnection poolConnection = PoolConnection.getInstance();
        poolConnection.initializes();
        sqlmxConn = poolConnection.getConnect();

        try
        {
            long tolerance = System.currentTimeMillis() + (60 * 1000 * 2);
            ExecutorService executorService = Executors.newFixedThreadPool(20);

            while (tolerance > System.currentTimeMillis())
            {
                Runnable worker = new ThreadSimulator();
                executorService.execute(worker);

                Thread.sleep(30);
            }

            querying();

        }
        catch (Exception e)
        {
            System.out.println("Error al completar ciclo, " + e.getMessage());
        }

    }

    public static void querying()
    {
        System.out.println("Intentando consultar tabla...");
        sqlmxQuery = "select * from desarrollo.transaccional.test1";
        Statement stmt;
        ResultSet res;

        try
        {
            stmt = sqlmxConn.createStatement();
            res = stmt.executeQuery(sqlmxQuery);

            while (res.next())
            {

                System.out.println(res.getInt("id"));
                System.out.println(res.getString("fecha_sys"));
                System.out.println(res.getString("hora_sys"));
            }

            res.close();
            stmt.close();
        }
        catch (Exception e)
        {
            System.out.println("Problemas al consultar la tabla " + e.getMessage());
        }
    }


    public static int createTable()
    {
        System.out.println("Intentando crear tabla...");
        String query = "create table  desarrollo.transaccional.test1(id largeint generated always as IDENTITY (start with 1 increment by 1 minvalue 1 no cycle) , fecha_sys DATE , hora_sys TIME , data1 varchar (10) DEFAULT '', ) ";
        Statement stmt;
        try
        {
            stmt = sqlmxConn.createStatement();
            int i = stmt.executeUpdate(query);
            stmt.close();
            return i;
        }
        catch (Exception e)
        {
            System.out.println("Problemas al crear tabla: " + e.getMessage());
            return -1;
        }
    }
}
