package com.ks;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class ThreadSimulator implements Runnable
{

    @Override
    public void run()
    {

        Connection connection = PoolConnection.getInstance().getConnect();
        System.out.println(" resultado de insert: " + insertInto(connection));

    }


    public static int insertInto(Connection connection)
    {
        System.out.print("Intentando insertar en tabla...");
        String sqlmxQuery = "insert into desarrollo.transaccional.test1(fecha_sys,hora_sys) values ( CURRENT_DATE ,CURRENT_TIME )";
        Statement stmt;

        try
        {
            stmt = connection.createStatement();
            int i = stmt.executeUpdate(sqlmxQuery);
            stmt.close();
            return i;
        }
        catch (Exception e)
        {
            System.out.println("Problemas al insertar: " + e.getMessage());
            return -1;
        }
        finally
        {
            try
            {
                connection.close();
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }
        }

    }
}
